package no.uib.ii.inf112;

public class TextAlignerClass implements TextAligner {

    /**
     * Center a string.
     * <p>
     * For example,
     *
     * <pre>
     * assertEquals("  A  ", textUtils.center("A", 5));
     * </pre>
     *
     * @param text  The string to be centered
     * @param width The width of the page
     * @return the centered text
     */



    @Override
    public String center(String text, int width) {
        StringBuilder s = new StringBuilder();



        int textLength = text.length();
        if (width < textLength) {
            throw new IllegalArgumentException("Width must be bigger than text length.");
        }
        int spaces = ((width - textLength) / 2);

        for (int i = 0; i < width-textLength; i++) {
            if (i == (spaces)) {
                s.append(text);
            }
            else {
                s.append(" ");
            }
        }
        return s.toString();
    }

    /**
     * Align string to the right.
     * <p>
     * For example,
     *
     * <pre>
     * assertEquals("    A", textUtils.flushRight("A", 5));
     * </pre>
     *
     * @param text  The string to be aligned
     * @param width The width of the page
     * @return the aligned text
     */
    @Override
    public String flushRight(String text, int width) {
        StringBuilder s = new StringBuilder();

        int textLength = text.length();
        if (width < textLength) {
            throw new IllegalArgumentException("Width must be bigger than text length.");
        }
        int spaces = (width - textLength);

        for (int i = 0; i < spaces; i++) s.append(" ");

        s.append(text);
        return s.toString();
    }

    /**
     * Align string to the left.
     * <p>
     * For example,
     *
     * <pre>
     * assertEquals("A    ", textUtils.flushLeft("A", 5));
     * </pre>
     *
     * @param text  The string to be aligned
     * @param width The width of the page
     * @return the aligned text
     */
    @Override
    public String flushLeft(String text, int width) {
        StringBuilder s = new StringBuilder();

        int textLength = text.length();
        if (width < textLength) {
            throw new IllegalArgumentException("Width must be bigger than text length.");
        }
        int spaces = (width - textLength);
        s.append(text);

        for (int i = 0; i < spaces; i++) s.append(" ");

        return s.toString();
    }

    /**
     * Justify text, so it uses the whole width.
     * <p>
     * Inserts extra spaces between words to make it fit the width.
     * <p>
     * For example,
     *
     * <pre>
     * assertEquals("fee   fie   foo", textUtils.justify("fee fie foo", 15));
     * </pre>
     *
     * @param text  The string to be aligned
     * @param width The width of the page
     * @return the aligned text
     */
    @Override
    public String justify(String text, int width) {
        return null;
    }
}
